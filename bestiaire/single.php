<?php get_header() ?>


<div class="container-single">

    <div class="wrapper-card">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title"><?php the_title(); ?></h2>
                <p class="card-toise">Taille &#8776; <?php the_field('taille'); ?> toises</p>
                <p class="card-text"><?php the_field('description'); ?></p>

            </div>
        </div>
    </div>

    <div class="wrapper-img">
        <?php the_post_thumbnail('medium', ['class' => 'img-single', 'alt' => 'image']) ?>
        <div class="wrapper-regime">
            <?php
            $featured_posts = get_field('regime_alimentaire');
            if( $featured_posts ): ?>
            <h3>Régime alimentaire :</h3>
            <ul>
                <?php foreach( $featured_posts as $post ): 
                setup_postdata($post); ?>
                <li>
                    <a class="mange-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </li>
                <?php endforeach; ?>
            </ul>
            <?php 
             // Reset the global post object so that the rest of the page works correctly.
            wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>

</div>

<?php get_footer() ?>