<?php

namespace App;

function bestiaire_supports (){
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    
}

function bestiaire_title_separator() {
    return '|';
}

function bestiaire_document_title_parts($title) {
    unset($title['tagline']);
    return $title;
}

function register_my_menu(){
    register_nav_menu( 'main-menu', 'Menu principal' );
}

function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-blocks-style' );
    if(! is_user_logged_in()){
        wp_deregister_style( 'dashicons' );
    } 
}

add_action( 'wp_enqueue_scripts', 'App\smartwp_remove_wp_block_library_css', 100 );
add_action( 'after_setup_theme', 'App\register_my_menu' );
add_action('after_setup_theme', 'App\bestiaire_supports');
add_filter('document_title_separator', 'App\bestiaire_title_separator');
add_filter('document_title_parts', 'App\bestiaire_document_title_parts')
?>

<!-- Export Custom fields -->
<?php 

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_62272130f0105',
        'title' => 'Beast',
        'fields' => array(
            array(
                'key' => 'field_6227214bde1be',
                'label' => 'taille',
                'name' => 'taille',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => 5,
            ),
            array(
                'key' => 'field_62279f284cacc',
                'label' => 'description',
                'name' => 'description',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ),
            array(
                'key' => 'field_62279f3c4cacd',
                'label' => 'Regime alimentaire',
                'name' => 'regime_alimentaire',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array(
                    0 => 'post',
                ),
                'taxonomy' => '',
                'filters' => array(
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
    ));
    
    endif;		

?>