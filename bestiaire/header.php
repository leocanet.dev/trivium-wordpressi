<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="premier site wordpress, incroyable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri();?>">
    <title><?php wp_title(); ?></title>
    <?php wp_head() ?>
</head>

<body>

    <header>
        <nav id="navigation" class="navbar navbar bg-dark">
            <h1>Le bestiaire</h1>
            <?php
            wp_nav_menu(
            array(
                'theme_location' => 'main-menu',
                'menu_id' => 'primary-menu',
                'menu_class' => 'menu',
            )
            );
            ?>
        </nav>
    </header>