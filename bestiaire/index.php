<?php get_header() ?>
<?php $args = array(
'orderby' => 'title',
'order' => 'ASC',
);
$query = new WP_Query( $args ); ?>

<?php if ( $query->have_posts() ) : ?>
<div class="container">

    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
    <div class="wrapper-card-index">
        <div class="card">
            <div class="card-body-index">
                <h2 class="card-title"><?php the_title(); ?></h2>
                <a class="card-detail" href="<?php the_permalink()?>">Détail de l'article </a>
            </div>
        </div>
    </div>

    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
</div>

<?php else : ?>
<p><?php _e( 'Aucun article ne correspond à votre recherche, veuillez créer des articles.' ); ?></p>
<?php endif; ?>
<?php get_footer() ?>